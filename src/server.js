// ----------------------------------------------------------------------------
// Copyright (c) Microsoft Corporation.
// Licensed under the MIT license.
// ----------------------------------------------------------------------------

let path = require('path');
const axios = require('axios');
var NestedKeys = require('nested-keys');

const express = require("express");
const bodyParser = require("body-parser");
const app = express();
const csv = require('csv-parser');
const fs = require('fs');
const xlsxFile = require('read-excel-file/node');

// Prepare server for Bootstrap, jQuery and PowerBI files
app.use('/js', express.static('./node_modules/bootstrap/dist/js/')); // Redirect bootstrap JS
app.use('/js', express.static('./node_modules/jquery/dist/')); // Redirect JS jQuery
app.use('/css', express.static('./node_modules/bootstrap/dist/css/')); // Redirect CSS bootstrap
app.use('/public', express.static('./public/')); // Use custom JS and CSS files

const port = process.env.PORT || 3001;

app.use(bodyParser.json());

app.use(bodyParser.urlencoded({
    extended: true
}));

app.get('/', function (req, res) {
    res.sendFile(path.join(__dirname + '/../views/index.html'));
});
app.post('/getFromCSV',  function(req, res, next) {

    const id = 1;
    const type = 2;
    const arr_row = [];
    let xl_row = [];
    let arr = [];
    const filter_row = [];
    const bName = req.body.bName;
    const uName = req.body.uName;


        if(type == 1){
            fs.createReadStream(path.join(__dirname, '/../public/Book1.csv'))
            .pipe(csv())
            .on('data', (row) => {    
                console.log(row);
                if(row.userid == id){
                     arr_row.push(row);
                }else{
                    filter_row.push(row);
                }             
            })
            .on('end', () => {
                console.log(`Parsed ${arr_row.length} rows`)
                res.status(200).send({msg:`Hi is this yours id ${id} and your data is`, data:arr_row});
             });
    
        }else if(type == 2){
            xlsxFile(path.join(__dirname, '/../public/Project_List.xlsx')).then((rows) => {
                rows.forEach( (col)=>{
                    if(col.includes(bName) && col.includes(uName)){
                        return res.status(200).send({status:true});
                    }else{
                        // console.log("I am here");
                        // res.status(400).send({status:false});
                    }
                    // col.forEach((data)=>{
                    //    arr.push(data);
                    // });                
                });
                return res.status(200).send({status:false});
            }).catch((e)=>{
                return {err:e};
            });
            
        }else{
             res.status(400).send({msg:"file type is not present"});
              
        }    
});


app.post('/getBots',  function(req, res, next) {

    let uName = req.body.uName;
    console.log(uName);
    const botArr = [];
    const resArr = [];
    xlsxFile(path.join(__dirname, '/../public/Project_List.xlsx')).then((rows) => {
        rows.forEach( (col)=>{
            if(col.includes(uName)){
                col.forEach((data)=>{
                    if(data !== null && !isNaN(parseInt(data))) 
                    botArr.push(data);
                });               
            }                           
        });
        for(let i = 0; i<botArr.length; i++){
            if(i % 2 !== 0)
            resArr.push(botArr[i]);
        }
        return res.status(200).send({bots:resArr});
    }).catch((e)=>{
        return {err:e};
    });       
});

app.post('/adddata',  (req, res) =>{
    var Excel = require('exceljs')

console.log(req.body)
    var workbook = new Excel.Workbook()
    var arr=[]
    var path ='././data.xlsx'
    if (fs.existsSync(path)) {
        console.log("sssssssss")

        workbook.xlsx.readFile('./data.xlsx')
        .then(function(){
            
         var worksheet = workbook.getWorksheet(1)
         let time=new Date()
    
       var row =[
           req.body.username,req.body.processname,req.body.jobid,time
    ]
     worksheet.addRow(row)
          return       workbook.xlsx.writeFile('./data.xlsx')
          
        })
        return res.status(200).send("successfully added");

    }
    else{
        let data = req.body;
        console.log(data);
        let worksheet = workbook.addWorksheet('data')
    
    worksheet.columns = [
      {header: ' UserName', key: 'username'},//username
      {header: 'ProcessName', key: 'processname'},//process name
      {header: 'JobId', key: 'jobid'},//jobid

      {header: 'Time Stamp', key: 'timestamp'},
    
    ]
    
    worksheet.getRow(1).font = {bold: true}
    let time=new Date()

    data.timestamp=time
    // Dump all the data into Excel
    worksheet.addRow(data)
    
    workbook.xlsx.writeFile('data.xlsx')
    
    return res.status(200).send("successfully added");

    }
})
app.post('/getxlsxdata',  function(req, res, next) {

    let username = req.body.uName;
    console.log(username);
    const botArr = [];
    const resArr = [];
    xlsxFile('data.xlsx').then((rows) => {
        console.log(rows,"ooy")
        rows.forEach( (col)=>{
            console.log(col,"iii")
            if(col.includes(username)){
               botArr.push(col)   
               console.log(botArr,"ee")
            }             
              
        });
        return res.status(200).send({data:botArr});

     
    }).catch((e)=>{
        return {err:e};
    });       
});

app.post('/sendmsg',  async function(req, res, next) {
    console.log(req.body)
    /* let r = (Math.random() + 1).toString(36).substring(7);
    console.log("random", r);*/
    
    const data = JSON.stringify({
        "userId":req.body.userID,
        "sessionId": req.body.userID,
        "text": req.body.MSG,
        "data": {
            "sender":"aa"
        }
    });
 
    const config = {
        method: 'post',
        url: 'https://endpoint-trial.cognigy.ai/53db5906f63a38927aa4c6bed1fb763d90557198c140d86ba10e999597952d44',
        headers: {
            'Content-Type': 'application/json'
        },
        data: data
    };
    console.log(config,"uu")
    let response1 = await axios(config)


    let resp = await normalizeResponse(response1.data, "aa")
    console.log(JSON.stringify(resp));
    // return resp
    return res.status(200).send(resp);

    // res.send(resp);

})

const normalizeResponse = async (response, input) => {
    console.log("response to normalize", response)
    let modifiedResp;
    if (response.text === "") {
        let resp = NestedKeys.get(response, ["data", "_cognigy", "_default", "_buttons"])
        modifiedResp = {
            "data": [{
                "recipient_id": input,
                "text": resp.text,
                "buttons": resp.buttons
            }]
        }
        return modifiedResp
    }
    else {
        return ({ "data": [{ "recipient_id": input.sender, "text": response.text }] })
    }
}

app.listen(port, () => console.log(`Listening on port ${port}`));